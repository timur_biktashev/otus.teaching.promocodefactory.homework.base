﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryEmployeeRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private List<T> Data { get; set; }

        public InMemoryEmployeeRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> CreateAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data.Add(entity);
            return Task.FromResult(entity.Id);
        }

        public Task DeleteAsync(Guid id)
        {
            Data.Remove(Data.Where(w => w.Id == id).First());
            return Task.CompletedTask;
        }

        public Task<Guid> UpdateAsync(T entity)
        {
            DeleteAsync(entity.Id);
            Data.Add(entity);
            return Task.FromResult(entity.Id);
        }
    }
}