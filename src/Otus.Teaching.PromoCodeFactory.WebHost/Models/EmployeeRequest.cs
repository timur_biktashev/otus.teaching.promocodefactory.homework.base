﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        [Required(ErrorMessage = "Укажите имя сотрудника")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Имя сотрудника должно быть минимум 3 символа и меньше 50 символов")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Укажите фамилию сотрудника")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Фамилия сотрудника должна быть минимум 2 символа и меньше 50 символов")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Укажите почту сотрудника")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Почта сотрудника должна быть минимум 5 символа и меньше 50 символов")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Укажите идентификатор роли сотрудника")]
        public List<Guid> Role { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Количество промокодов должно быть положительным")]
        public int AppliedPromocodesCount { get; set; }
    }
}
