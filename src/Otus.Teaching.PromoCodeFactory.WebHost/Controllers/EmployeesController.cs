﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<EmployeeShortResponse>>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return Ok(employeesModelList);
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return Ok(employeeModel);
        }

        /// <summary>
        /// Добавить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync([FromBody] EmployeeRequest entity)
        {

            try
            {
                var roleList = await _roleRepository.GetAllAsync();

                var entityRoles = entity.Role;

                if (!await IsRoleExists(entity.Role))
                {
                    var exceptGuids = entityRoles.Except(roleList.Select(s => s.Id).Where(w => entityRoles.Contains(w)));

                    var sb = new StringBuilder();

                    foreach (var item in exceptGuids)
                    {
                        sb.Append("\n");
                        sb.Append(item);
                    }

                    return NotFound($"Роли не найдены: {sb.ToString()}");
                }

                var roleEntities = roleList.Where(w => entityRoles.Contains(w.Id));

                var dbEntity = new Employee()
                {
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    Email = entity.Email,
                    Roles = roleEntities.ToList(),
                    AppliedPromocodesCount = entity.AppliedPromocodesCount
                };

                var resultGuid = await _employeeRepository.CreateAsync(dbEntity);
                return Ok(resultGuid);
            }
            catch (Exception ex)
            {
                return BadRequest(entity);
            }
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Guid>> UpdateEmployeeAsync([FromBody] EmployeeRequest entity, [FromRoute] Guid id)
        {
            try
            {
                var employee = await _employeeRepository.GetByIdAsync(id);
                if (employee == null)
                {
                    return NotFound();
                }

                var roleList = await _roleRepository.GetAllAsync();

                var entityRoles = entity.Role;

                if (!await IsRoleExists(entity.Role))
                {
                    var exceptGuids = entityRoles.Except(roleList.Select(s => s.Id).Where(w => entityRoles.Contains(w)));

                    var sb = new StringBuilder();

                    foreach (var item in exceptGuids)
                    {
                        sb.Append("\n");
                        sb.Append(item);
                    }

                    return NotFound($"Роли не найдены: {sb.ToString()}");
                }

                var roleEntities = roleList.Where(w => entityRoles.Contains(w.Id));

                employee.FirstName = entity.FirstName;
                employee.LastName = entity.LastName;
                employee.Email = entity.Email;
                employee.Roles = roleEntities.ToList();
                employee.AppliedPromocodesCount = entity.AppliedPromocodesCount;

                var resultGuid = await _employeeRepository.UpdateAsync(employee);
                return Ok(resultGuid);
            }
            catch (Exception)
            {
                return BadRequest(entity);
            }
        }

        /// <summary>
        /// Удалить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Guid>> UpdateEmployeeAsync([FromRoute] Guid id)
        {
            try
            {
                var employee = await _employeeRepository.GetByIdAsync(id);
                if(employee == null)
                {
                    return NotFound();
                }

                await _employeeRepository.DeleteAsync(id);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Проверка наличия ролей в БД
        /// </summary>
        /// <param name="entityRoles">Роли сотрудника</param>
        /// <returns>Наличие роли в БД</returns>
        private async Task<bool> IsRoleExists (List<Guid> entityRoles)
        {
            var roleList = await _roleRepository.GetAllAsync();

            var employeeRoles = roleList.Select(s => s.Id)
                                        .Where(w => entityRoles.Contains(w));

            if (entityRoles.Count != employeeRoles.Count())
            {
                return false;
            }

            return true;
        }
    }
}